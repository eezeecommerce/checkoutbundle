<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/07/16
 * Time: 10:10
 */

namespace eezeecommerce\CheckoutBundle\Tests\Functional;


use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use eezeecommerce\CartBundle\Storage\SessionStorage;

class CheckoutTest extends WebTestCase
{
    /**
     * Client
     *
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient(
        );

        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $session = new Session(new MockArraySessionStorage());
        $session->set("_cart", $manager);
    }

    public function testCheckoutPageReturnsSuccess()
    {
        $this->client->request("GET", "/checkout/address");

        $this->assertEquals("200", $this->client->getResponse()->getStatusCode());
    }

    public function testSubmitFormWithNoDataRedirects()
    {
        $crawler = $this->client->request("GET", "/checkout/address");

        $buttonCrawlerNode = $crawler->selectButton("proceed");

        $form = $buttonCrawlerNode->form();

        $this->client->submit($form);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testSubmitFormWithInvalidEmailRedirects()
    {
        $crawler = $this->client->request("GET", "/checkout/address");

        $buttonCrawlerNode = $crawler->selectButton("proceed");

        $form = $buttonCrawlerNode->form(array(), "POST");

        $data = array(
            "eezeecommerce_orderbundle_orders" => array(
                "billing_address" => array(
                    "name_first" => "asd",
                    "name_last" => "asd",
                    "address_1" => "asd",
                    "address_2" => "asd",
                    "city" => "asd",
                    "county" => "asd",
                    "postcode" => "asd",
                    "contact_email" => "asd@",
                    "contact_phone" => "asd",
                )
            )
        );

        $this->client->submit($form, $data);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->getContainer()->get("session")->getFlashBag();

    }
}