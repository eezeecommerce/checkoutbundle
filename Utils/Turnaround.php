<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31/03/16
 * Time: 12:02
 */

namespace eezeecommerce\CheckoutBundle\Utils;


use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Turnaround
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var integer
     */
    private $min;

    /**
     * @var integer
     */
    private $max;

    public function __construct(SessionInterface $session, ObjectManager $em)
    {
        $this->session = $session;
        $this->em = $em;
    }

    public function setSettings(SettingsProvider $settings)
    {
        $settings = $settings->loadSettingsBySite();
        $this->min = $settings->getMinTurnaroundTime();
        $this->max = $settings->getMaxTurnaroundTime();
    }

    public function getTurnaround()
    {
        $min = 0;
        $max = 0;

        if ($this->session->has("_cart")) {
            $cart = $this->session->get("_cart");
            foreach ($cart as $orderline) {

                $product = $orderline->getEntity();

                if (null !== $product) {
                    if (null === $product->getTurnaroundMaxOverride() || null === $product->getTurnaroundMinOverride()) {
                        return array("min" => $this->min, "max" => $this->max);
                    }
                    else {
                        if ($min < $product->getTurnaroundMinOverride()) {
                            $min = $product->getTurnaroundMinOverride();
                        }
                        if ($max < $product->getTurnaroundMaxOverride()) {
                            $max = $product->getTurnaroundMaxOverride();
                        }
                    }
                }

            }
        }

        return array("min" => $min, "max" =>  $max);
    }
}