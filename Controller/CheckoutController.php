<?php
/**
 * PHP version 5.6
 *
 * @category Controller
 * @package  EezeecommerceCheckoutBundle
 * @author   Liam Sorsby <liam@eezeecommerce.com>
 * @author   Daniel Sharp <dan@eezeecommerce.com>
 * @license  commercial https://www.eezeecommerce.com/terms
 * @link     https://www.eezeecommerce.com
 */

namespace eezeecommerce\CheckoutBundle\Controller;

use eezeecommerce\PaymentBundle\Entity\GatewayConfig;
use eezeecommerce\PaymentBundle\Form\Type\Payment\PaymentType;
use eezeecommerce\PaymentBundle\Form\Type\Payment\PaypalProType;
use eezeecommerce\ShippingBundle\Entity\Country;
use eezeecommerce\UserBundle\Entity\Address;
use eezeecommerce\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\OrderBundle\Entity\Orders;
use eezeecommerce\OrderBundle\Form\OrdersType;

/**
 * Class CheckoutController
 *
 * @category Controller
 * @package  EezeecommerceCheckoutBundle
 * @author   Liam Sorsby <liam@eezeecommerce.com>
 * @author   Daniel Sharp <dan@eezeecommerce.com>
 * @license  commercial https://www.eezeecommerce.com/terms
 * @link     https://www.eezeecommerce.com
 */
class CheckoutController extends Controller
{
    /**
     * Login action
     *
     * @param Request $request Request object
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|
     *         \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $route = $request->attributes->get("_route");
        if ($this->getUser()) {
            $user = $this->getUser();
            if (!$user instanceof User) {
                return $this->redirectToRoute("_eezeecommerce_checkout_address");
            }
            $manager = $this->get("eezeecommerce.order_manager");
            if (null === ($order = $manager->getOrder())) {
                return $this->redirectToRoute("_eezeecommerce_checkout_user");
            }
            $entity = $order->getEntity();
            if (null !== $entity->getBillingAddress()
                && null !== $entity->getShippingAddress()
            ) {
                return $this->redirectToRoute("_eezeecommerce_checkout");
            }
            return $this->redirectToRoute("_eezeecommerce_checkout_user");
        }
        $cart = $this->get("eezeecommerce.cart");

        if ($cart->isEmpty()) {
            $this->addFlash(
                'warning',
                "Your cart is empty please add items to your 
                cart before attempting to proceed to the checkout."
            );
            return $this->redirectToRoute("_eezeecommerce_cart");
        }

        return $this->render(
            "AppBundle:Frontend/Checkout:login.html.twig", [
                "route" => $route
            ]
        );
    }

    /**
     * Checkout if user is logged in
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|
     *         \Symfony\Component\HttpFoundation\Response
     */
    public function userAction()
    {
        if ($this->getUser()) {
            $user = $this->getUser();
            if (null !== ($addresses = $user->getAddress())) {
                $defaultShip = false;
                $defaultInv = false;
                foreach ($addresses as $address) {
                    if (!$address instanceof Address) {
                        break;
                    }
                    if ($address->getBillingPrimary()) {
                        $defaultInv = $address;
                    }
                    if ($address->getShippingPrimary()) {
                        $defaultShip = $address;
                    }
                    if ($defaultShip && $defaultInv) {
                        break;
                    }
                }
                $manager = $this->get("eezeecommerce.order_manager");
                if ($defaultShip && $defaultInv) {
                    $order = new Orders();
                    $order->setBillingAddress($defaultInv);
                    $order->setShippingAddress($defaultShip);
                    $manager->setOrder($order);
                    return $this->redirectToRoute("_eezeecommerce_checkout");
                }
            }

        }
        return $this->redirectToRoute("_eezeecommerce_checkout_address");
    }

    /**
     * Checkout
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|
     *         \Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction(Request $request)
    {
        $cart = $this->get("eezeecommerce.cart");

        if ($cart->isEmpty()) {
            $this->addFlash(
                'warning',
                "Your cart is empty please add items to your 
                cart before attempting to proceed to the checkout."
            );
            return $this->redirectToRoute("_eezeecommerce_cart");
        }

        $order = $this->get("eezeecommerce.order_manager");

        if (!$order->hasOrder()) {
            $this->addFlash(
                'warning',
                "Please choose enter a delivery address 
                before attempting to proceed."
            );
            return $this->redirectToRoute("_eezeecommerce_checkout_login");
        }
        $em = $this->getDoctrine()->getManager();
        if (null !== $id = $order->getOrder()->getEntity()->getId()) {
            $orderEntity = $this->getDoctrine()->getRepository(Orders::class)
                ->find($id);

            $shippingCountry = $orderEntity
                ->getShippingAddress()
                ->getCountry();

            $billingCountry = $orderEntity
                ->getBillingAddress()
                ->getCountry();

            $order->getOrder()
                ->getEntity()
                ->setShippingAddress(
                    $orderEntity
                    ->getShippingAddress()
                );
            $order->getOrder()
                ->getEntity()
                ->setBillingAddress(
                    $orderEntity
                    ->getBillingAddress()
                );

            $order->getOrder()->getEntity()
                ->getShippingAddress()->setCountry($shippingCountry);
            $order->getOrder()->getEntity()
                ->getBillingAddress()->setCountry($billingCountry);
        } else {
            $shippingCountry = $em->merge(
                $order->getOrder()
                    ->getEntity()
                    ->getShippingAddress()
                    ->getCountry()
            );
            $billingCountry = $em->merge(
                $order->getOrder()
                    ->getEntity()
                    ->getBillingAddress()
                    ->getCountry()
            );
            $order->getOrder()->getEntity()
                ->getShippingAddress()->setCountry($shippingCountry);
            $order->getOrder()->getEntity()
                ->getBillingAddress()->setCountry($billingCountry);
        }

        $shipping = $this->get("eezeecommerce_shipping.shipping.manager");
        $shipping->setWeight($cart->getWeight());
        $shipping->setCountryCode(
            $order->getOrder()
                ->getEntity()
                ->getShippingAddress()
                ->getCountry()
                ->getCode()
        );
        $shipping->setPostcode(
            $order->getOrder()
                ->getEntity()
                ->getShippingAddress()
                ->getPostcode()
        );
        $shipping->setTotal($cart->total());

        if (null === $shipping->getResults()) {
            $this->addFlash(
                'danger', "Unfortunately there are no shipping 
                options available to this specific country. 
                Please contact sales if you believe this to be an error."
            );
            return $this->redirectToRoute("_eezeecommerce_checkout_address");
        }

        $turnaround = $this->get("eezeecommerce_checkout.utils.turnaround");

        $form = $this->createForm(new PaymentType(), null, array(
            "action" => $this->generateUrl("_eezeecommerce_payment_prepare"),
            "method" => "POST"
        ));

        $paypalPro = $this->getDoctrine()->getRepository("eezeecommercePaymentBundle:GatewayConfig")
            ->findOneBy(array("factoryName" => "paypal_pro_checkout"));
        if ($paypalPro instanceof GatewayConfig) {
            if ($paypalPro->getEnabled()) {
                $form->add("card", new PaypalProType());
            }
        }

        $form->handleRequest($request);

        return $this->render(
            "AppBundle:Frontend/Checkout:checkout.html.twig", [
                "order" => $order->getOrder()->getEntity(),
                "cart" => $cart,
                "form" => $form->createView(),
                "shipping" => $shipping->getResults(),
                "turnaround" => $turnaround->getTurnaround()
            ]
        );
    }

    /**
     * Type In Address
     *
     * @param Request $request Request Object
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|
     *         \Symfony\Component\HttpFoundation\Response
     */
    public function addressAction(Request $request)
    {
        $cart = $this->get("eezeecommerce.cart");
        $orderManager = $this->get("eezeecommerce.order_manager");
        $em = $this->getDoctrine()->getManager();

        if ($cart->isEmpty()) {
            $this->addFlash(
                'warning',
                "Your cart is empty please add items to
                your cart before attempting to proceed to the checkout."
            );
            return $this->redirectToRoute("_eezeecommerce_cart");
        }

        if ($orderManager->hasOrder() && null === $this->getUser()) {
            $order = $orderManager->getOrder()->getEntity();
            $shipping = $em->merge($order->getShippingAddress());
            $billing = $em->merge($order->getBillingAddress());
            if ($order->getBillingAddress()->getCountry() instanceof Country) {
                if ($order->getShippingAddress()->getCountry() instanceof Country) {
                    $shippingCountry = $em->merge(
                        $order->getShippingAddress()->getCountry()
                    );
                } else {
                    $shippingCountry = $em->merge(
                        $order->getBillingAddress()->getCountry()
                    );
                }
                $billingCountry = $em->merge(
                    $order->getBillingAddress()->getCountry()
                );
                $order->setShippingAddress($shipping);
                $order->setBillingAddress($billing);
                $order->getShippingAddress()->setCountry($shippingCountry);
                $order->getBillingAddress()->setCountry($billingCountry);
            } else {
                $order = new Orders();
            }
        } else {
            $order = new Orders();
        }

        $form = $this->createForm(new OrdersType(), $order);

        if ("POST" === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (!empty($form['vat_number']->getData())) {
                    $validator = $this->get("vat.service");
                    $vatNumber = $form['vat_number']->getData();
                    $vatNumber = strtoupper(preg_replace("/\s/", "", $vatNumber));
                    $match = preg_match("/^[A-z]{2}+[0-9]+/", $vatNumber);

                    if (!$match) {
                        $error = "Your vat number is in an incorrect format.
                         Please try again using the following format: GB123456789";
                        $this->addFlash("warning", $error);
                        return $this->render(
                            "AppBundle:Frontend/Checkout:address.html.twig", [
                                "form" => $form->createView()
                            ]
                        );
                    }

                    $valid = $validator->validate($vatNumber);
                    if (!$valid) {
                        $error = "You have entered an incorrect vat number.";
                        $this->addFlash("warning", $error);
                        $order->setVatNumber("");
                    }
                }

                if (null !== ($user = $this->getUser())) {
                    $order->getShippingAddress()->setUser($user);
                    $order->getBillingAddress()->setUser($user);
                }

                $orderManager = $this->get("eezeecommerce.order_manager");

                $orderManager->setOrder($order);

                return $this->redirectToRoute("_eezeecommerce_checkout");
            }
        }

        return $this->render(
            "AppBundle:Frontend/Checkout:address.html.twig", [
                "form" => $form->createView()
            ]
        );
    }
}
